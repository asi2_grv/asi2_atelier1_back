package com.cpe.springboot.user.controller;

import com.cpe.springboot.Transaction;
import com.cpe.springboot.user.model.UserModel;
import org.apache.activemq.command.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

@Component
public class UserBusListener {
    @Autowired
    private UserService userService;

    @Autowired
    JmsTemplate jmsTemplate;

    @JmsListener(destination = "${spring-messaging.queues.user}", containerFactory = "connectionFactory")
    public void receiveUserData(Transaction<UserModel> transaction, Message message) {
        switch (transaction.action) {
            case "update":
                System.out.println("[UserBusListener] RECEIVED User MSG=[" + transaction.data + "]");
                userService.updateUser(transaction.data);
                break;
            //TODO: other cases for other actions
            default:
                System.out.println("Unknown User action in queue (Ignored)");
                break;
        }
    }

}
