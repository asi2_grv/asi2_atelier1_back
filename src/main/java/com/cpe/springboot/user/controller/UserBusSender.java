package com.cpe.springboot.user.controller;

import com.cpe.springboot.Transaction;
import com.cpe.springboot.user.model.UserModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

@Service
public class UserBusSender {

    @Value("${spring-messaging.queues.user}")
    private String userQueueName;
    @Autowired
    JmsTemplate jmsTemplate;

    public void update(UserModel user) {
        System.out.println("[UserBusSender] SEND String MSG=[" + user + "]");
        jmsTemplate.convertAndSend(userQueueName, new Transaction<>("update", user));
    }
}
