package com.cpe.springboot;

import com.cpe.springboot.user.controller.UserService;
import com.cpe.springboot.user.model.UserModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jms.DefaultJmsListenerContainerFactoryConfigurer;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.event.EventListener;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.config.JmsListenerContainerFactory;
import org.springframework.jms.core.JmsTemplate;

import javax.jms.ConnectionFactory;
import java.util.Optional;

@EnableJms
@SpringBootApplication
public class CardMngMonolithicApplication {

    @Autowired
    JmsTemplate jmsTemplate;

    @Bean
    public JmsListenerContainerFactory<?> connectionFactory(ConnectionFactory connectionFactory,
                                                            DefaultJmsListenerContainerFactoryConfigurer configurer) {
        DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();
        // This provides all boot's default to this factory, including the message converter
        configurer.configure(factory, connectionFactory);
        // You could still override some of Boot's default if necessary.

        //enable topic mode
        factory.setPubSubDomain(true);
        return factory;
    }


    /**
     * Executed after application start
     */
    @EventListener(ApplicationReadyEvent.class)
    public void doInitAfterStartup() {
        //enable to be in topic mode! to do at start
        jmsTemplate.setPubSubDomain(true);
    }

    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(CardMngMonolithicApplication.class, args);

        // Test sequence:

        // Create test User (using service directly
        UserModel initialUser = new UserModel();
        initialUser.setLogin("login_test");
        initialUser.setPwd("pwd_test");
        initialUser.setLastName("lastname_test");
        initialUser.setSurName("surname_test");
        initialUser.setEmail("email_test");
        initialUser.setAccount(1337);

        UserService userService = context.getBean(UserService.class);
        userService.addUser(initialUser);

        // Update test User (using queue)
        UserModel testUser = new UserModel();
        testUser.setId(initialUser.getId());
        testUser.setLastName("lastname_test_edited");
        testUser.setSurName("surname_test_edited");
        testUser.setEmail("email_test_edited");

        userService.updateUserAsync(testUser);

        Optional<UserModel> resultUser = userService.getUser(testUser.getId());
        //User isn't update yet, as the queue hasn't been consumed
        try {
            Thread.sleep(5000);
        } catch (Exception e) {
            e.printStackTrace();
        }
        //User is updated correctly, as the queue has been consumed 5s later
        resultUser = userService.getUser(testUser.getId());
        System.out.println("resultUser=[" + resultUser + "]");
    }
    /* CURL update async test: (or use postman)

        curl --location --request PUT '127.0.0.1:8082/user/6' \
        --header 'Content-Type: application/json' \
        --data-raw '{
            "lastName":"lastname_test_edited_postman",
            "surName":"surname_test_edited_postman",
            "email":"email_test_edited_postman"
        }
        '

    * */

}
