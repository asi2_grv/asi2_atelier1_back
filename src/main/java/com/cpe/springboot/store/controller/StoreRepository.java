package com.cpe.springboot.store.controller;

import com.cpe.springboot.store.model.StoreModel;
import org.springframework.data.repository.CrudRepository;

public interface StoreRepository extends CrudRepository<StoreModel, Integer> {


}
