package com.cpe.springboot;

import java.io.Serializable;

public class Transaction<T> implements Serializable {
    public String action;
    public T data;

    public Transaction(String action, T data) {
        this.action = action;
        this.data = data;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

}
