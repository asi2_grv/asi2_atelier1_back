package com.cpe.adhoc;

import com.cpe.springboot.user.model.UserModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


//ONLY FOR TEST NEED ALSO TO ALLOW CROOS ORIGIN ON WEB BROWSER SIDE
@CrossOrigin
@RestController
public class AdHocController {

    @Autowired
    private UserBusSender userBusSender;

    @RequestMapping(method = RequestMethod.GET, value = "/test")
    private void test() {
        //Get the test user
        UserModel testUser = new UserModel();
        testUser.setId(6);
        testUser.setLastName("lastname_test_edited_adhoc");
        testUser.setSurName("surname_test_edited_adhoc");
        testUser.setEmail("email_test_edited_adhoc");

        //Write a predefined data to the ESB
        userBusSender.update(testUser);
    }

}
